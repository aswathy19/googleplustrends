import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by aswathy on 8/5/15.
 */
public class HttpURLConnectionConfiguration extends Configuration {
    private String mainurl;
    private String trendurl;
    LinkedHashMap<String, String> list;
    public  HttpURLConnectionConfiguration()
    {
        Yaml yaml = new Yaml();
        InputStream input = null;
        try {
            input = new FileInputStream(new File("googleapi.yml"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        list = (LinkedHashMap<String, String>) yaml.load(input);
        this.mainurl=list.get("mainurl");
        this.trendurl=list.get("trendurl");
    }
    public String getMainurl()
    {
        return mainurl;
    }
    public String getTrendurl()
    {
        return trendurl;
    }
    public void setMainurl(String mainurl)
    {
            this.mainurl=mainurl;
    }
    public void setTrendurl(String trendurl)
    {
        this.trendurl=trendurl;
    }

}
