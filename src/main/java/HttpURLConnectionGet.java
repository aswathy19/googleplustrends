import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.yaml.snakeyaml.Yaml;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by aswathy on 7/5/15.
 */
public class HttpURLConnectionGet {

    private final String USER_AGENT = "Mozilla/5.0";
    private String mainurl;
    private String trendurl;

    JSONObject sendGet(HttpURLConnectionConfiguration connectionConfiguration) throws Exception {
        Jedis jedis = new Jedis("107.178.219.43 ", 5866);
        jedis.auth("ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5");
        System.out.println("Connected to Redis");
        System.out.println("!!!!!!");
        JSONArray jarray3=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        JSONArray jsonArray=new JSONArray();
        mainurl=connectionConfiguration.getMainurl();
        trendurl=connectionConfiguration.getTrendurl();
        URL obj = new URL(mainurl);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String result;
        result=response.toString();
        JSONObject out = new JSONObject(result);
        JSONArray jarray=out.getJSONObject("responseData").getJSONObject("feed").getJSONArray("entries");

        for(int i=0;i<jarray.length();i++)
        {
            String title = jarray.getJSONObject(i).getString("title");
            title=title.replaceAll("\\s+", "");
            System.out.println("!"+title);
            String url2=trendurl+title;
            URL obj2 = new URL(url2);
            HttpURLConnection con2 = (HttpURLConnection) obj2.openConnection();
            con2.setRequestMethod("GET");
            con2.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode2 = con2.getResponseCode();

            BufferedReader in2 = new BufferedReader(
                    new InputStreamReader(con2.getInputStream()));
            String inputLine2;
            StringBuffer response2 = new StringBuffer();

            while ((inputLine2 = in2.readLine()) != null) {
                response2.append(inputLine2);
            }
            in2.close();

            String result2;
            result2=response2.toString();

            JSONObject out2 = new JSONObject(result2);
            JSONArray jarray2=out2.getJSONArray("items");

            for(int j=0;j<jarray2.length();j++)
            {
                try {
                    String ur = jarray2.getJSONObject(j).getJSONObject("object").getJSONArray("attachments").getJSONObject(0).getString("url");
                    boolean flag=(ur.contains("plus.google.com"))||(ur.contains("youtube.com"))||(ur.length()<40);
                    System.out.println(ur);
                    if(!flag)
                    {

                       // jsonArray.put(ur);
                        jedis.sadd("links",ur);

                    }
                }catch(JSONException e)
                {
                    System.out.println("");

                }
            }

        }

        //jsonObject.put("url",jsonArray);

        System.out.println(jsonObject);
        return jsonObject;

    }

}
